-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 09 mars 2022 à 15:26
-- Version du serveur :  5.7.24
-- Version de PHP : 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `beerdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `beer`
--

CREATE TABLE `beer` (
  `idBeer` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `createdDate` date NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `idCompany` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `beer`
--

INSERT INTO `beer` (`idBeer`, `name`, `createdDate`, `description`, `idCompany`, `photo`) VALUES
(1, 'Heineken', '2022-03-08', 'Bière industrielle', 1, '1.jpg'),
(2, 'Jolicoeur Hivernale', '2022-03-08', 'Robe ambrée soutenue, corps malté intense, saveur subtile aux notes de pain d’épices.', 2, 'JOL-Hivernale-75_.jpg'),
(3, 'Jolicoeur l\'estivale', '2022-03-08', 'Blonde légère et rafraichissante à l’arôme citronné prononcé.', 2, 'JOL-Lestivale.jpg'),
(4, 'Jolicoeur Miel', '2022-03-08', 'Une bière éphémère, une brassée avec le miel de saison récolté par l’apiculteur voisin de la brasserie !', 2, 'JOL-Miel-33_.jpg'),
(5, 'Litote', '2022-03-08', 'Bière blonde type Saison, légèrement acidulée, bien équilibrée et légère.', 3, 'SEP-Litote-33.jpg'),
(6, 'Oxymore', '2022-03-08', 'Bière acidulée à la mûre sauvage. OUI ! c’est fruité et NON, c’est pas sucré !', 3, 'SEP-Oxymore-3-33.jpg'),
(7, 'Parataxe', '2022-03-08', 'Bière brune intense mais légère en bouche. Les notes torréfiées de café typiques des stouts sont adoucies par une présence plus importante des houblons utilisés.', 3, 'SEP-Parataxe-33.jpg'),
(8, 'Hyperbole', '2022-03-08', 'Une IPA à l’européene, et même à la française, puisqu’elle met en avant un houblon original : le très frenchy « barbe rouge ». Sur une base de grains robuste pleine de seigle (mais adoucie d’une touche de malt caramélisé).', 3, 'SEP-Hyperbole-44__.jpg'),
(9, 'Its cold in Alaska', '2022-03-08', 'Bière légère.', 4, 'BAR-Its-cold-in-Alaska-33_.jpg'),
(10, 'Sugar Kane', '2022-03-08', 'Bière légère sur le céréale finement houblonné.', 4, 'BAR-Sugar-Kane-33_.jpg'),
(11, 'GoldFinger', '2022-03-08', 'Bière maltée et épicée. Bel équilibre entre le malt et le houblon. Le houblon utilisé est le Goldings UK. C’est une sorte d’ambrée sèche et ronde.', 4, 'BAR-Goldfinger-33_.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `company`
--

CREATE TABLE `company` (
  `idCompany` int(11) NOT NULL,
  `companyName` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `siret` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `department` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `company`
--

INSERT INTO `company` (`idCompany`, `companyName`, `description`, `siret`, `website`, `department`) VALUES
(1, 'Heineken', NULL, NULL, 'https://www.heinekenfrance.fr/', '72'),
(2, 'Jolicoeur', NULL, NULL, 'https://www.bierejolicoeur.com', '72'),
(3, 'Septante-Deux', NULL, NULL, 'https://brasserieseptantedeux.com', '72'),
(4, 'Baribale', NULL, NULL, 'https://www.bieresbretonnes.fr/brasseries/bra', '44');

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

CREATE TABLE `note` (
  `idNote` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `idBeer` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `comment` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `note`
--

INSERT INTO `note` (`idNote`, `value`, `id_user`, `idBeer`, `date`, `comment`) VALUES
(1, 1, 1, 1, '2022-03-08', 'vraiment pas ouf'),
(2, 2, 2, 1, '2022-03-08', 'peut dépanner'),
(3, 5, 1, 2, '2022-03-08', 'trés rafraichissante en hiver'),
(4, 4, 2, 2, '2022-03-08', 'trés bonne bière de saison'),
(5, 5, 1, 3, '2022-03-08', 'trés rafraichissante en été'),
(6, 4, 2, 3, '2022-03-08', 'trés bonne bière de saison'),
(7, 4, 1, 4, '2022-03-08', 'Goût original'),
(8, 3, 2, 4, '2022-03-08', 'un peu spéciale'),
(9, 4, 1, 5, '2022-03-08', 'trés goûtue'),
(10, 3, 2, 5, '2022-03-08', 'efficace'),
(11, 3, 1, 6, '2022-03-08', ''),
(12, 2, 2, 6, '2022-03-08', ''),
(13, 3, 1, 7, '2022-03-08', ''),
(14, 4, 2, 7, '2022-03-08', 'trés bonne !'),
(15, 5, 1, 8, '2022-03-08', 'MEILLEURE BIERE DE TOUS LES TEMPS !!'),
(16, 4, 2, 8, '2022-03-08', 'simplement divine !'),
(17, 4, 1, 9, '2022-03-08', 'rien de plus à dire'),
(18, 4, 2, 9, '2022-03-08', 'trés bonne !'),
(19, 4, 1, 10, '2022-03-08', 'Trés hydratante'),
(20, 3, 2, 10, '2022-03-08', 'Manque un peu de goût'),
(21, 5, 1, 11, '2022-03-08', 'Surprenante'),
(22, 3, 2, 11, '2022-03-08', ''),
(23, 5, NULL, NULL, '2022-03-08', 'AA'),
(24, 5, 1, 1, '2022-03-08', 'AA'),
(25, 5, 1, 1, '2022-03-08', 'AA'),
(26, 5, 1, 1, '2022-03-09', 'AA'),
(27, 5, 1, 1, '2022-03-09', 'AA'),
(28, 5, 1, 1, '2022-03-09', 'AA'),
(29, 5, 1, 1, '2022-03-09', 'AA'),
(30, 5, 1, 1, '2022-03-09', 'AA'),
(31, 5, 1, 1, '2022-03-09', 'AA'),
(32, 5, 1, 1, '2022-03-09', 'AA'),
(33, 1, 1, 1, '2022-03-09', 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'),
(34, 1, 1, 2, '2022-03-09', 'Nul'),
(35, 1, 1, 2, '2022-03-09', 'SPAM'),
(36, 5, 1, 3, '2022-03-09', 'Absolument génial'),
(37, 5, 1, 5, '2022-03-09', 'MAGNIFIQUE');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL,
  `pseudo` varchar(45) DEFAULT NULL,
  `roles` json DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`, `pseudo`, `roles`, `phone`, `photo`, `name`, `firstname`, `description`) VALUES
(1, 'mathis@mathis.com', '$2y$13$tynSFJDK/dRJiqnbdakTKO7ES3r5rhOXq32ZGBAv5aFQuDOWSb86e', 'Mathisdu72', NULL, '066666666', NULL, 'Mathis', 'mathis', 'Etre superieur'),
(2, 'clement@clement.com', '$2y$13$SBbhRjkG3frmT7ggR4XDIObaMy9g45fvjKVPqDt7ri061TvgnpyDS', 'Clement', NULL, '066666667', NULL, 'clement', 'clement', ''),
(3, 'justin@justin.com', '$2y$13$SBbhRjkG3frmT7ggR4XDIObaMy9g45fvjKVPqDt7ri061TvgnpyDS', 'justin', NULL, '066666667', NULL, 'justin', 'justin', ''),
(4, 'pierre_jean@pierre_jean.com', '$2y$13$SBbhRjkG3frmT7ggR4XDIObaMy9g45fvjKVPqDt7ri061TvgnpyDS', 'Pierre_jean', NULL, '066666667', NULL, 'Pierre jean', 'Pierre jean', ''),
(5, 'josselin@josselin.com', '$2y$13$tynSFJDK/dRJiqnbdakTKO7ES3r5rhOXq32ZGBAv5aFQuDOWSb86e', 'Josselin', NULL, '066666667', NULL, 'josselin', 'josselin', ''),
(6, 'test@test.com', '$2y$13$tynSFJDK/dRJiqnbdakTKO7ES3r5rhOXq32ZGBAv5aFQuDOWSb86e', 'Test', '[]', NULL, NULL, NULL, NULL, ''),
(7, 'test2@test.com', '$2y$13$9dL7ORCaNWz87qI0CaagvOpgcLTuiTZ1XM2xzkYTOB5GIbMMgskKG', 'AAAAA', '[]', NULL, NULL, NULL, NULL, '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `beer`
--
ALTER TABLE `beer`
  ADD PRIMARY KEY (`idBeer`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `company_idx` (`idCompany`);

--
-- Index pour la table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`idCompany`);

--
-- Index pour la table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`idNote`),
  ADD KEY `fk_Note_Beer1_idx` (`idBeer`),
  ADD KEY `fk_Note_User1_idx` (`id_user`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `beer`
--
ALTER TABLE `beer`
  MODIFY `idBeer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `company`
--
ALTER TABLE `company`
  MODIFY `idCompany` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `note`
--
ALTER TABLE `note`
  MODIFY `idNote` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `beer`
--
ALTER TABLE `beer`
  ADD CONSTRAINT `company_id` FOREIGN KEY (`idCompany`) REFERENCES `company` (`idCompany`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `fk_Note_Beer1` FOREIGN KEY (`idBeer`) REFERENCES `beer` (`idBeer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Note_User1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
