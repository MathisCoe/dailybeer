php bin/console cache:clear 
php bin/console doctrine:database:drop --force 
php bin/console doctrine:database:create
php bin/console make:migration
php bin/console doctrine:migrations:migrate  --no-interaction
#php vendor/phpunit/phpunit/phpunit --testdox