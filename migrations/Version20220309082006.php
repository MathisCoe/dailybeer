<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309082006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE beer (idBeer INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) DEFAULT NULL, createdDate DATE NOT NULL, description VARCHAR(45) DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, idCompany INT DEFAULT NULL, INDEX company_idx (idCompany), UNIQUE INDEX name_UNIQUE (name), PRIMARY KEY(idBeer)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company (idCompany INT AUTO_INCREMENT NOT NULL, companyName VARCHAR(45) NOT NULL, description VARCHAR(45) DEFAULT NULL, siret VARCHAR(45) DEFAULT NULL, website VARCHAR(45) DEFAULT NULL, department VARCHAR(20) DEFAULT NULL, PRIMARY KEY(idCompany)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE note (idNote INT AUTO_INCREMENT NOT NULL, value INT NOT NULL, date DATE DEFAULT NULL, comment VARCHAR(1000) DEFAULT NULL, idBeer INT DEFAULT NULL, id_user INT DEFAULT NULL, INDEX fk_Note_User1_idx (idBeer), INDEX fk_Note_Beer1_idx (id_user), PRIMARY KEY(idNote)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id_user INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id_user)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE beer ADD CONSTRAINT FK_58F666AD5F0662BD FOREIGN KEY (idCompany) REFERENCES company (idCompany)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA142B0B3833 FOREIGN KEY (idBeer) REFERENCES beer (idBeer)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14FE6E88D7 FOREIGN KEY (id_user) REFERENCES user (id_user)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA142B0B3833');
        $this->addSql('ALTER TABLE beer DROP FOREIGN KEY FK_58F666AD5F0662BD');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14FE6E88D7');
        $this->addSql('DROP TABLE beer');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE note');
        $this->addSql('DROP TABLE user');
    }
}
