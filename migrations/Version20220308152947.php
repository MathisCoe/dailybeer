<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220308152947 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE note DROP CONSTRAINT FK_CFBDFA14FE6E88D7');
        $this->addSql('ALTER TABLE note DROP CONSTRAINT FK_CFBDFA142B0B3833');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14FE6E88D7 FOREIGN KEY (idUser) REFERENCES "user" (idUser) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA142B0B3833 FOREIGN KEY (idBeer) REFERENCES beer (idBeer) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE note DROP CONSTRAINT fk_cfbdfa142b0b3833');
        $this->addSql('ALTER TABLE note DROP CONSTRAINT fk_cfbdfa14fe6e88d7');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT fk_cfbdfa142b0b3833 FOREIGN KEY (idbeer) REFERENCES "user" (iduser) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT fk_cfbdfa14fe6e88d7 FOREIGN KEY (iduser) REFERENCES beer (idbeer) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
