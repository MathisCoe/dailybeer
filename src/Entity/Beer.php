<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Beer
 *
 * @ORM\Table(name="beer", uniqueConstraints={@ORM\UniqueConstraint(name="name_UNIQUE", columns={"name"})}, indexes={@ORM\Index(name="company_idx", columns={"idCompany"})})
 * @ORM\Entity
 */
class Beer
{
    /**
     * @var int
     *
     * @ORM\Column(name="idBeer", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idbeer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createddate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=true)
     */
    private $description;

      /**
     * @var string|null
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;
    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idCompany", referencedColumnName="idCompany")
     * })
     */
    private $idcompany;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function getIdbeer(): ?int
    {
        return $this->idbeer;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreateddate(): ?\DateTimeInterface
    {
        return $this->createddate;
    }

    public function setCreateddate(\DateTimeInterface $createddate): self
    {
        $this->createddate = $createddate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->idcompany;
    }

    public function setCompany(?Company $idcompany): self
    {
        $this->idcompany = $idcompany;

        return $this;
    }


  


}
