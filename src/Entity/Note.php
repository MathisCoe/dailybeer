<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table(name="note", indexes={@ORM\Index(name="fk_Note_User1_idx", columns={"idBeer"}), @ORM\Index(name="fk_Note_Beer1_idx", columns={"id_user"})})
 * @ORM\Entity
 */
class Note
{
    /**
     * @var int
     *
     * @ORM\Column(name="idNote", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idnote;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=1000, nullable=true)
     */
    private $comment;

    /**
     * @var \Beer
     *
     * @ORM\ManyToOne(targetEntity="Beer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idBeer", referencedColumnName="idBeer")
     * })
     */
    private $idbeer;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user")
     * })
     */
    private $id_user;

    public function getIdnote(): ?int
    {
        return $this->idnote;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getBeer(): ?Beer
    {
        return $this->idbeer;
    }

    public function setBeer(?Beer $id_user): self
    {
        $this->idbeer = $id_user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->id_user;
    }

    public function setUser(?User $idbeer): self
    {
        $this->id_user = $idbeer;

        return $this;
    }


}