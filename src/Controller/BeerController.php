<?php

namespace App\Controller;

use App\Entity\Beer;
use App\Form\BeerType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/beer")
 */
class BeerController extends AbstractController
{
    /**
     * @Route("/", name="app_beer_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $beers = $entityManager
            ->getRepository(Beer::class)
            ->findAll();

        return $this->render('beer/index.html.twig', [
            'beers' => $beers,
        ]);
    }

    /**
     * @Route("/{idbeer}", name="app_beer_show", methods={"GET"})
     */
    public function show(Beer $beer): Response
    {
        return $this->render('beer/show.html.twig', [
            'beer' => $beer,
        ]);
    }

    /**
     * @Route("/{idbeer}/edit", name="app_beer_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Beer $beer, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BeerType::class, $beer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_beer_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('beer/edit.html.twig', [
            'beer' => $beer,
            'form' => $form,
        ]);
    }
}
