<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\Mapping\Entity;
use App\Entity\Beer;
use App\Entity\Company;
use App\Entity\Note;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $beers = $em->getRepository(Beer::class)->findAll();
        return $this->render('home/index.html.twig', [
            'beers' => $beers
        ]);
    }
      /**
     * @Route("/connexion", name="app_connexion")
     */
    public function connexion(): Response
    {
        return $this->render('home/connexion.html.twig', [
        
        ]);
    }
 
     /**
     * @Route("/inscription/post", name="app_inscription_post")
     */
    public function inscription_post(Request $request): Response
    {

        return $this->render('home/inscription.html.twig', [
         
        ]);
    }
    /**
     * @Route("/fiche/{idBeer}", name="app_fichebiere")
     * requirements={"idBeer":"[0-9]+",}
     */
    public function fiche_biere(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $idBeer =$request->attributes->get('idBeer',null);


        if ($token =  $this->get('security.token_storage')->getToken()) { 
            $user = $token->getUser(); 
            if (is_object($user)) { 
                $notesUser = $em->getRepository(Note::class)->findBy(array('idbeer' => $idBeer,'id_user' => $user));
                $nbNoteUser = count($notesUser);
            } 
        }else{
            $nbNoteUser = NULL;
        } 
        $idBeer =$request->attributes->get('idBeer',null);
        $beer = $em->getRepository(Beer::class)->find($idBeer);
        $notes = $em->getRepository(Note::class)->findBy(array('idbeer' => $idBeer));
        
        $moyenne = 0;
        $nbNotes = count($notes);
        foreach ($notes as $note) {
            
           $val = $note->getValue();
            $moyenne = $moyenne + $val;
        
        }
        if($nbNotes != 0){
            $moyenne = $moyenne / $nbNotes;
        }else{
            $moyenne = 0;
        }
        
        return $this->render('home/fiche_biere.html.twig', [
            'beer' => $beer,
            'moyenne' => $moyenne,
            'notes' => $notes,
            'nbNoteUser' => $nbNoteUser,
            'notesUser' => $notesUser
        ]);
    }

    /**
     * @Route("/creerNote", name="app_fichebiere_creerNote")
     * 
     */
    public function creerNote(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $note = new Note();
        $value =$request->request->get('note',null);
        $note->setValue($value);
        $note->setComment($request->request->get('comment',null));
        $idBeer = $request->request->get('idBeer',null);
        $beer = $em->getRepository(Beer::class)->find($idBeer);
        $note->setBeer($beer);
        $id_user = $request->request->get('id_user',null);
        $user = $em->getRepository(User::class)->find($id_user);
        $note->setUser($user);
        $date = new \DateTime('@'.strtotime('now'));
        
        $note->setDate($date);
        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($note);

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();


        return $this->redirectToRoute('app_fichebiere', ['idBeer' => $idBeer]);
    }
    
    /**
     * @Route("/supprimerNote", name="app_fichebiere_supprimerNote")
     * 
     */
    public function supprimerNote(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $idNote = $request->request->get('idNote',null);
        $note = $em->getRepository(Note::class)->find($idNote);
        $em->remove($note);
        $em->flush();
        $idBeer =$request->request->get('idBeer',null);
        return $this->redirectToRoute('app_fichebiere', ['idBeer' => $idBeer]);

    }
     // il faudra mettre l'id de l'user
     /**
     * @Route("/profil/{id_user}", name="app_info_profil")
     */
    public function info_profil(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $id_user =$request->attributes->get('id_user',null);
        $user = $em->getRepository(User::class)->find($id_user);
        $notes =  $em->getRepository(Note::class)->findBy(array('id_user' => $user->getid_user() ));

        return $this->render('home/info_profil.html.twig', [
            'user' => $user,
            'notes' => $notes,
        ]);
    }
     // il faudra mettre l'id de l'user
     /**
     * @Route("/config", name="app_config")
     */
    public function config_profil(): Response
    {
        return $this->render('home/config_profil.html.twig', [
        ]);
    }

    // il faudra mettre l'id de l'user
     /**
     * @Route("/recherche/{saisie}", name="app_recherche")
     */
    public function recherche(Request $request): Response
    {
        $saisie =$request->attributes->get('saisie',null);
        return $this->render('home/recherche.html.twig', [
            'saisie' => $saisie,
        ]);
    }

    public function getmoyenne(Beer $beer): ?int
    {
        $em = $this->getDoctrine()->getManager();

        $notes = $em->getRepository(Note::class)->findBy(array('idbeer' => $beer->getIdbeer()));
        $moyenne = 0;
        $nbNotes = count($notes);
        foreach ($notes as $note) {

            $val = $note->getValue();
            $moyenne = $moyenne + $val;
        }
        if ($nbNotes != 0) {
            $moyenne = $moyenne / $nbNotes;
        } else {
            $moyenne = 0;
            return   $moyenne;
        }
    }
}
